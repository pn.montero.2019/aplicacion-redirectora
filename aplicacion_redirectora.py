#!/usr/bin/python

import socket
import random

# Definimos una lista de URLs
list_urls = [
    "https://www.google.com/",
    "https://www.youtube.com/",
    "https://www.urjc.es/"]


# El servidor web está a la espera de nuevas conexiones entrantes de los clientes,
# Una vez que se establece la conexión entre el cliente y el servidor, el servidor
# comienza a leer los datos enviados por el cliente, después de recibir y procesar
# la solicitud del cliente, el servidor genera una respuesta, que generalmente es una página HTML.

# PASO2: defino una función que me gestione las solicitudes
def handle_request(request):
    # Verifico que la solicitud es una solicitud GET válida
    if request.startswith("GET"):
        # Elegimos una URL aleatoria de la lista de URLs externas
        redirect_url = random.choice(list_urls)

        # Ahora creamos la respuesta de redirección con el código 302
        response = f"HTTP/1.1 302 Found\r\nLocation: {redirect_url}\r\n\r\n"
    else:
        # si la solicitud no es GET, responder con un mensaje de error 400
        response = "HTTP/1.1 400 Bad Request\r\nContent-Type: text/html\r\n\r\n"
        response += "<html><body><h1>400 Bad Request</h1></body></html>"
    return response.encode()


def main():
    # PASO1: Creamos el socket y lo vinculamos al puerto
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.bind(('localhost', 1234))
    mySocket.listen(5)  #esperamos máximo hasta 5 colas de conecciones TCP

    try:
        while True:
            print("Waiting for connections...")
            (recvSocket, address) = mySocket.accept()  # guarda ip y puerto del cliente
            print("HTTP request received:")
            request = recvSocket.recv(2048).decode()
            print(request)
            # Llamo a la función para manejar la solicitud
            response = handle_request(request)
            # Envio la respuesta al cliente
            recvSocket.send(response)
            recvSocket.close()
    except KeyboardInterrupt:
        print("Closing binded socket")
        mySocket.close()


if __name__ == "__main__":
    main()
